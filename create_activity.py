# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 10:15:31 2019

@author: Raquel
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 15:11:46 2019

@author: Pedro
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Aug 13 14:58:44 2018

@author: Pedro
"""
from datetime import datetime
import json
import csv

def convert_date_to_unix_time(date):
    return datetime.fromisoformat(date).timestamp()

def main():
    file = 'anon.log'
    fd = open(file)
    idx = 0
    count_events = 0
    activity_id = 0
    with open('activity_data.csv','w') as file:
        wr = csv.writer(file, dialect='excel')
        header = ['activity_id', 'date', 'subject_id', 'subject_type', 'activity_type', 'object_id', 'object_type']
        wr.writerow(header)
        while 1:
            type_ok = False
            line = fd.readline()
            if not line:
                break

            data = json.loads(line)
            count_events += 1

            # VARIABLES
            username = data.get('username')
            event_type = data.get('event_type')
            unix_time = convert_date_to_unix_time(data.get('time'))
            date = datetime.utcfromtimestamp(unix_time)
            event = data.get('event')
            context = data.get('context')
            subject_id = context.get("user_id")
            subject_type = None
            activity_type = None
            object_id = None
            object_type = None

            try:
                # FILTRADO DEL USERNAME
                if username != '':
                    
                    # FORO
                    if 'edx.forum.comment.created' == event_type or 'edx.forum.response.created' == event_type: # 73
                         type_ok = True
                         #ev = json.loads(str(event))
                         activity_id += 1
                         subject_type = "user"
                         activity_type = "create"
                         object_id = event.get("id")
                         object_type = "comment"
                         
                    elif 'edx.forum.thread.created' == event_type: # 140
                         type_ok = True
                         activity_id += 1
                         subject_type = "user"
                         activity_type = "create"
                         object_id = event.get("id")
                         object_type = "post"

                    elif 'edx.forum.response.voted' == event_type or 'edx.forum.thread.voted' == event_type:
                         type_ok = True
                         activity_id += 1
                         subject_type = "user"
                         activity_type = "create"
                         object_id = event.get("id")
                         object_type = "like"
                         
                    elif 'edx.forum.thread.viewed' == event_type:
                         type_ok = True
                         activity_id += 1
                         subject_type = "user"
                         activity_type = "view"
                         object_id = event.get("id")
                         object_type = "post"
                         
                # Si el tipo está entre los admitidos, se graba la información
                if type_ok == True:
                    idx += 1

                    variables = [activity_id, date, subject_id, subject_type, activity_type, object_id,
                              object_type]
                    wr.writerow(variables)

            except ValueError:
                    pass

        file.close()
    print (idx)
    print (count_events)
    print ('Finished')

if __name__ == "__main__":
    main()
