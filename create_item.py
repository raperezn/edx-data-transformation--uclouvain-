#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 17 10:49:20 2019

@author: raquel
"""


# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 15:11:46 2019

@author: Pedro
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Aug 13 14:58:44 2018

@author: Pedro
"""
import json
import csv

def main():
    file = 'anon.log'
    fd = open(file)
    idx = 0
    count_events = 0
    with open('item_data.csv','w') as file:
        wr = csv.writer(file, dialect='excel')
        header = ['item_id', 'item_type', 'parent_id', 'parent_type', 'value']
        wr.writerow(header)
        while 1:
            type_ok = False
            line = fd.readline()
            if not line:
                break
            
            data = json.loads(line)
            count_events += 1

            # VARIABLES
            username = data.get('username')
            event_type = data.get('event_type')
            event = data.get('event')
            #context = data.get('context')
            #subject_id = context.get("user_id")
            item_id = None
            item_type = None
            parent_type = None
            parent_id = None
            value = None
            response = None
            discussion = None

            try:
                # FILTRADO DEL USERNAME
                if username != '':
                    
                    # FORO
                    if 'edx.forum.comment.created' == event_type: # 73
                         type_ok = True
                         #ev = json.loads(str(event))
                         item_id =  event.get("id")
                         item_type = "comment"
                         response = event.get("response")
                         parent_id = response.get("id")
                         parent_type = "comment"
                         value = event.get("body")
                         
                    elif 'edx.forum.response.created' == event_type:
                         type_ok = True
                         #ev = json.loads(str(event))
                         item_id =  event.get("id")
                         item_type = "comment"
                         discussion = event.get("discussion")
                         parent_id = discussion.get("id")
                         parent_type = "post"
                         value = event.get("body")
                         
                    elif 'edx.forum.thread.created' == event_type: # 140
                         type_ok = True
                         #ev = json.loads(str(event))
                         item_id =  event.get("id")
                         item_type = "post"
                         parent_id = None
                         parent_type = None
                         value = event.get("body")
                         
                    elif 'edx.forum.thread.voted' == event_type:
                         type_ok = True
                         #ev = json.loads(str(event))
                         item_id =  event.get("id")
                         item_type = "like"
                         parent_id = event.get("commentable_id")
                         parent_type = "post"
                         value = event.get("vote_value")
                    
                    elif 'edx.forum.response.voted' == event_type:
                         type_ok = True
                         #ev = json.loads(str(event))
                         item_id =  event.get("id")
                         item_type = "like"
                         parent_id = event.get("commentable_id")
                         parent_type = "comment"
                         value = event.get("vote_value")
                       
                # Si el tipo está entre los admitidos, se graba la información
                if type_ok == True:
                    idx += 1

                    variables = [item_id, item_type, parent_id,
                              parent_type, value]
                    wr.writerow(variables)

            except ValueError:
                    pass

        file.close()
    print (idx)
    print (count_events)
    print ('Finished')

if __name__ == "__main__":
    main()